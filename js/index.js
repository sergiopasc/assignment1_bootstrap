
$(function(){
	$("[data-toggle='tooltip']").tooltip()
	$("[data-toggle='popover']").popover()
	$('.carousel').carousel({
				interval:2000
		})
	$('#registro').on('show.bs.modal', function(e){
		console.log('el modal registro se comienza a abrir');
		$('#registroBtn').removeClass('btn-outline-success');
		$('#registroBtn').addClass('btn-primary');
		$('#registroBtn').prop('disabled', true);
		
	});
	$('#registro').on('shown.bs.modal', function(e){
		console.log('el modal registro se terminó de abrir');
	});
	$('#registro').on('hide.bs.modal', function(e){
		console.log('el modal registro comienza a ocultarse');
	});
	$('#registro').on('hidden.bs.modal', function(e){
		console.log('el modal registro se terminó de ocultar');
		$('#registroBtn').removeClass('btn-primary');
		$('#registroBtn').addClass('btn-outline-success');
		$('#registroBtn').prop('disabled', false);
	});
});
